import React, {FC} from 'react';
import classes from "./Preloader.module.scss";
import classNames from "classnames";


interface IProps {

}
const Preloader:FC<IProps> = ({}) => {
    return (
        <div className={classNames({
            [classes.Preloader]: true
        })}>
            Preloader
        </div>
    );
};

export default Preloader;
