import React, {FC} from 'react';
import classes from "./Wrapper.module.scss";
import classNames from "classnames";


interface IProps {
    children?: React.ReactNode
}
const Wrapper:FC<IProps> = ({children}) => {
    return (
        <div className={classNames({
            [classes.Wrapper]: true
        })}>
            {children}
        </div>
    );
};

export default Wrapper;
