import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {IMessage, IUserData} from "../../types";



interface IInitialStateTypes {
    isLoggedIn: boolean
    loginData: IUserData | null
    messagesList: IMessage[]
    isFetching: boolean
    chatID: string | null
}

const initialState: IInitialStateTypes = {
    isLoggedIn: false,
    loginData: null,
    messagesList: [],
    isFetching: false,
    chatID: null
}


export const mainSlice = createSlice({
    name: 'main',
    initialState: initialState,
    reducers: {
        setIsLoggedIn(state: IInitialStateTypes = initialState, action: PayloadAction<boolean>): void {
            state.isLoggedIn = action.payload
        },
        setLoginDataToStore(state: IInitialStateTypes = initialState, action: PayloadAction<IUserData | null>): void {
            debugger
            state.loginData = action.payload
        },
        setMessages(state: IInitialStateTypes = initialState, action: PayloadAction<IMessage[]>): void {
            state.messagesList = [...state.messagesList, ...action.payload]
        },
        setChatId(state: IInitialStateTypes = initialState, action: PayloadAction<string>): void {
            state.chatID = action.payload
        },
        setToggleFetching(state: IInitialStateTypes = initialState, action: PayloadAction<boolean>): void {
            state.isFetching = action.payload
        },
    }
})

export const { setIsLoggedIn, setLoginDataToStore, setMessages, setChatId, setToggleFetching } = mainSlice.actions

export default mainSlice.reducer

