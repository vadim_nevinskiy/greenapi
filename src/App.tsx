import React, {lazy, Suspense} from 'react';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import {Preloader} from "./components/common";
import {Chat, Enter, NotFound} from "./pages";
import classNames from "classnames";
import classes from "./App.module.scss";


function App() {
  return (
      <div className={classNames({
          [classes.App]: true
      })}>
          <BrowserRouter>
            <Suspense fallback={<Preloader />}>
              <Routes>
                <Route path="/" element={<Enter />} />
                <Route path="/contacts" element={<Chat />} />
                <Route path="*" element={<NotFound />} />
              </Routes>
            </Suspense>
          </BrowserRouter>
      </div>
  );
}

export default App;
