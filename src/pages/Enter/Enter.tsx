import React, {FC, useEffect, useRef} from 'react';
import classes from "./Enter.module.scss";
import classNames from "classnames";
import { useNavigate } from "react-router-dom";
import {Wrapper} from "../../components/common";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../../store";
import {setLoginDataToStore, setIsLoggedIn} from "../../store/reducers/mainSlice";

interface IProps {

}
const Enter:FC<IProps> = ({}) => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const idInstanceRef = useRef<HTMLInputElement>(null);
    const apiTokenInstanceRef = useRef<HTMLInputElement>(null);
    const chatIdRef = useRef<HTMLInputElement>(null);
    const isLoggedIn = useSelector((state: RootState) => state.main.isLoggedIn);

    const submitForm = () => {
        if(idInstanceRef.current && apiTokenInstanceRef.current && chatIdRef.current){
            dispatch(setIsLoggedIn(true));
            dispatch(
                setLoginDataToStore (
                {
                        idInstance: idInstanceRef.current.value,
                        apiTokenInstance: apiTokenInstanceRef.current.value,
                        chatId: chatIdRef.current.value
                    }
                )
            )
        }
    }

    useEffect(() => {
        if(isLoggedIn) {
            const timeout = setTimeout(() => navigate('/contacts'), 300);
            return () => {
                clearTimeout(timeout);
            };
        }
    }, [isLoggedIn])

    return (
        <Wrapper>
            <div className={classNames({
                [classes.Enter]: true
            })}>
                <div className={classNames({
                    [classes.Enter__Form]: true
                })}>
                    <h3>Enter</h3>
                        <div className={classNames({
                            [classes.Enter__Form__Field]: true,
                            ["input-field"]: true
                        })}>
                            <input id="idInstance" type="text" className="validate" ref={idInstanceRef} />
                            <label htmlFor="idInstance">idInstance</label>
                        </div>
                        <div className={classNames({
                            [classes.Enter__Form__Field]: true,
                            ["input-field"]: true
                        })}>
                            <input id="apiTokenInstance" type="text" className="validate" ref={apiTokenInstanceRef} />
                            <label htmlFor="apiTokenInstance">apiTokenInstance</label>
                        </div>
                        <div className={classNames({
                            [classes.Enter__Form__Field]: true,
                            ["input-field"]: true
                        })}>
                            <input id="phone" type="text" className="validate" ref={chatIdRef} />
                            <label htmlFor="phone">Phone number</label>
                        </div>
                        <a
                            className="waves-effect waves-light btn"
                            onClick={submitForm}
                        >Enter</a>
                </div>
            </div>

            <div style={{fontWeight: 'bold', marginTop: 20, textAlign: 'center'}}>
                <div>
                    7103831282
                </div>
                <div>
                    895d93a21ed244ad81ed37ddcca11102c8c9b0b1d7cf44ed81
                </div>
                <div>
                    998901758454
                </div>
            </div>
        </Wrapper>
    );
};

export default Enter;
