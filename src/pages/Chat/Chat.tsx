import React, {FC, useRef, useState} from 'react';
import classes from "./Chat.module.scss";
import classNames from "classnames";
import {Wrapper} from "../../components/common";


interface IProps {

}
const Chat:FC<IProps> = ({}) => {
    const messageRef = useRef<HTMLInputElement>(null);
    const [message, setMessage] = useState<string | null>(null)



    const updateMessage = () => {
        if(messageRef.current) {
            if(messageRef.current.value) {
                setMessage(messageRef.current.value);
            }
        }
    }
    const sendMessage = () => {

    }

    return (
            <Wrapper>
                <div className={classNames({
                    [classes.Chat]: true
                })}>
                    <div className={classNames({
                        [classes.Chat__Form]: true
                    })}>
                        <div className={classNames({
                            [classes.Chat__ChatBody]: true
                        })}>

                        </div>
                        <div className={classNames({
                            [classes.Chat__MessageForm]: true
                        })}>
                            <div className={classNames({
                                [classes.Chat__MessageForm__FieldBlock]: true,
                                ["input-field"]: true
                            })}>
                                <input
                                    id="phone"
                                    type="text"
                                    className={classNames({
                                        ["validate"]: true,
                                        [classes.Chat__MessageForm__FieldBlock__TextField]: true
                                    })}
                                    ref={messageRef}
                                    onChange={updateMessage}
                                />
                                <label htmlFor="phone">Message</label>
                            </div>
                            <div className={classNames({
                                [classes.Chat__MessageForm__ButtonBlock]: true,
                            })}>
                                <i className={classNames({
                                    ["material-icons"]: true,
                                    [classes.Chat__MessageForm__Button]: true
                                })} onClick={sendMessage}>send</i>
                            </div>
                        </div>
                    </div>
                </div>
            </Wrapper>
    );
};

export default Chat;
