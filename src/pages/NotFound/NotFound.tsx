import React, {FC} from 'react';
import classes from "./NotFound.module.scss";
import classNames from "classnames";


interface IProps {

}
const NotFound:FC<IProps> = ({}) => {
    return (
        <div className={classNames({
            [classes.NotFound]: true
        })}>
            NotFound
        </div>
    );
};

export default NotFound;
